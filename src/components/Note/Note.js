import React, {Component} from 'react';
import './Note.css';

class Note extends Component {
  state = {
    value: this.props.value,
    editMode: false
  };

  onValueChange = event => {
    this.setState({value: event.target.value});
  };

  turnEditMode = () => {
    this.setState({editMode: true});
  };

  turnOffEditMode = () => {
    this.props.saveNote(this.props.id, this.state.value);
    this.setState({editMode: false});
  };

  render() {
    return (
      <div className="Note">
        <textarea
          rows="5"
          value={this.state.value}
          onChange={this.onValueChange}
          disabled={!this.state.editMode}
        />
        <div className="Note-buttons">
          <button
            className="Note-saveBtn"
            hidden={!this.state.editMode}
            onClick={this.turnOffEditMode}
          >
            Save
          </button>
          <button
            className="Note-editBtn"
            hidden={this.state.editMode}
            onClick={this.turnEditMode}
          >
            Edit
          </button>
          <button
            className="Note-deleteBtn"
            onClick={() => this.props.deleteNote(this.props.id)}
          >
            Delete
          </button>
        </div>
      </div>
    );
  }
}

export default Note;