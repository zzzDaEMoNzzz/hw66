import React from 'react';
import './ToWatchAdd.css';

const ToWatchAdd = ({inputValue, onChange, onSubmit}) => {
  return (
    <form className="ToWatchAdd" onSubmit={onSubmit}>
      <input
        type="text"
        value={inputValue}
        onChange={onChange}
        placeholder="Add new film"
      />
      <button>Add</button>
    </form>

  );
};

export default ToWatchAdd;
