import React, {Fragment} from 'react';
import './Preloader.css';

const Preloader = ({show}) => {
  if (!show) return null;
  return (
    <Fragment>
      <div className="Preloader" />
    </Fragment>
  );
};

export default Preloader;
