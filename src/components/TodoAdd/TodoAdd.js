import React from 'react';
import './TodoAdd.css';

const TodoAdd = ({inputValue, inputOnChange, formOnSubmit}) => {
  return (
    <form className="TodoAdd" onSubmit={formOnSubmit}>
      <input
        type="text"
        placeholder="Add new task"
        value={inputValue}
        onChange={inputOnChange}
      />
      <button>Add</button>
    </form>
  );
};

export default TodoAdd;
