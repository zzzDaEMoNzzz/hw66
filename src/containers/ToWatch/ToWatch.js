import React, {Component} from 'react';
import axios from 'axios';
import ToWatchAdd from "../../components/ToWatchAdd/ToWatchAdd";
import ToWatchList from "../../components/ToWatchList/ToWatchList";
import withLoaderHandler from "../../hoc/withLoader";

class ToWatch extends Component {
  state = {
    films: [],
    inputValue: '',
  };

  inputOnChange = event => {
    this.setState({
      inputValue: event.target.value
    })
  };

  getFilms = () => {
    axios.get('towatch.json').then(response => {
      if (response.data && Object.keys(response.data).length > 0) {
        const films = Object.keys(response.data).reduce((array, id) => {
          array.push({id, ...response.data[id]});
          return array;
        }, []);

        this.setState({films});
      } else this.setState({films: []});
    });
  };

  addFilm = event => {
    event.preventDefault();

    axios.post('towatch.json', {
      film: this.state.inputValue
    }).then(() => {
      this.setState({inputValue: ''});
      this.getFilms();
    });
  };

  deleteFilm = id => {
    axios.delete(`towatch/${id}.json`).then(() => {
      this.getFilms();
    });
  };

  saveChanges = (id, film) => {
    axios.put(`towatch/${id}.json`, {
      film
    });
  };

  componentDidMount() {
    this.getFilms();
  }

  render() {
    return (
      <div>
        <ToWatchAdd
          inputValue={this.state.inputValue}
          onChange={this.inputOnChange}
          onSubmit={this.addFilm}
        />
        <ToWatchList
          films={[...this.state.films].reverse()}
          deleteFilm={this.deleteFilm}
          saveChanges={this.saveChanges}
        />
      </div>
    );
  }
}

export default withLoaderHandler(ToWatch, axios);