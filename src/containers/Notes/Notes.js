import React, {Component} from 'react';
import axios from 'axios';
import NotesAdd from "../../components/NotesAdd/NotesAdd";
import NotesList from "../../components/NotesList/NotesList";
import withLoaderHandler from "../../hoc/withLoader";

class Notes extends Component {
  state = {
    notes: [],
    textAreaValue: '',
  };

  textAreaOnChange = event => {
    this.setState({textAreaValue: event.target.value});
  };

  addNote = event  => {
    event.preventDefault();

    axios.post('notes.json', {
      note: this.state.textAreaValue
    }).then(() => {
      this.setState({textAreaValue: ''});
      this.getNotes();
    });
  };

  getNotes = () => {
    axios.get('notes.json').then(response => {
      if (response.data && Object.keys(response.data).length > 0) {
        const notes = Object.keys(response.data).map(id => {
          return {id: id, ...response.data[id]};
        });

        this.setState({notes});
      } else {
        this.setState({notes: []});
      }
    });
  };

  saveNote = (id, note) => {
    axios.put(`notes/${id}.json`, {
      note
    });
  };

  deleteNote = id => {
    axios.delete(`notes/${id}.json`).then(() => {
      this.getNotes();
    });
  };

  componentDidMount() {
    this.getNotes();
  }

  render() {
    return (
      <div>
        <NotesAdd
          textAreaValue={this.state.textAreaValue}
          onChange={this.textAreaOnChange}
          onSubmit={this.addNote}
        />
        <NotesList
          notes={this.state.notes}
          saveNote={this.saveNote}
          deleteNote={this.deleteNote}
        />
      </div>
    );
  }
}

export default withLoaderHandler(Notes, axios);